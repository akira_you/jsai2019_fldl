#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  

Created on Tue Nov 20 19:08:29 2018

@author: La-noda
"""

import numpy as np
import util
nofType=2
d=np.load("data2/haipla.npz")
inputs=d['inputs'].astype(np.float32)#(-1,1,1024,1)
inputs=inputs.reshape(-1,1024)
tars_txt=d['tars_txt']
tars=util.mtar(tars_txt,nofType=nofType)
del d

fil=np.load("getSpec/filter.npy")
if(False):
    fil2=np.load("getSpec_old/filter.npy")
    fil=np.r_[fil,fil2]
#mizumasi
if(False):
    fa=np.copy(fil)
    fb=np.copy(fil)
    #fa[:300]=0
    #fb[300:]=0
    fa=fil**0.5
    fb=fil**2
    fil=np.r_[fil,fa,fb]
if(False):
   n=fil.shape[0] 
   nfil=[]
   for i in range(n):
       for j in range(n):    
           if(False):
               for k in range(n):
                   if(i<=j<=k):
                       nfil.append(fil[i]*fil[j]*fil[k])
           else:
               if(i<=j):
                   nfil.append(fil[i]*fil[j])
           
   fil=np.array(nfil)


filtered=inputs.dot(fil.transpose())

np.savez("filtered.npz",inputs=filtered.astype(np.float32),tars=tars)
