"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


"""
import functools
import operator

from chainer.functions import matmul,reshape,sum,exp,broadcast_to,softplus,relu
from chainer import initializers
from chainer import link
from chainer import variable,UpdateRule
import numpy as np


class FilterMix(link.Link):

    def __init__(self, fil, out_size,initialW=None):
        super(FilterMix, self).__init__()
        self.out_size = out_size
        self.filter_size = fil.shape[0]
        self.in_size = fil.shape[1]
        if(initialW is None):
            initialW=np.random.randn(self.out_size, self.filter_size)*0.3+1
        #self.fil.update_rule.enabled=False
        #Input and filter are log inten.
        # med[batchsize,outsize,insize]= (  filter[filtersize,in_size] X W[ouysize,filtersize]) + input[bachsize,in_size]  
        # return sum(exp(med),axis=on_insize)[batchsize,outsize]

        with self.init_scope():
            W_initializer = initializers._get_initializer(initialW)
            self.W = variable.Parameter(W_initializer)
            self.fil=variable.Parameter(fil)
            self._initialize_params()
        self.fil.update_rule=UpdateRule()
        self.fil.update_rule.enabled=False
        self.W.update_rule=UpdateRule()
        self.W.update_rule.enabled=False
       
    def _initialize_params(self):
        self.W.initialize((self.out_size, self.filter_size))
        
    def __call__(self, x):
        #def forward(self, x, n_batch_axes=1):
        if self.W.data is None:
            self._initialize_params()

        self.fil.update_rule.enabled=False        
        med= matmul(relu(self.W),self.fil) #[outsize,insize]
        med = reshape(med,(1,med.shape[0],med.shape[1] ))+reshape(x,(x.shape[0],1,x.shape[1] ))
        
        return sum( exp(med),axis=2) #[batchsize,outisze]
    
