# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


@date: 2016/9/7 (original) last update (2017/12)
@author: a-noda

CAUTION:
This is only for single GPU or CPU.
Because this dose not suport add_grad for awGrad
        and TrainToProp only see main GPU
"""

import chainer.functions as F
import cupy
import numpy as np
from chainer import cuda,Variable,Link
import chainer
class Aw(Link): #for AW
    def __init__(self,awLen=0):
        super().__init__();
        self.awd=0
        
        self.updateRate=0.1
        self.gamma=0
        self.decay=0
        self.L1decay=0
        self.normModel=2  #1=L1 norm 2=L2Norm other=**2 model   1and 3 are almost same result
        self.eps=1e-10
        self.limit=1e+10
        self.pthresh=6
        self.pthresh_base=0
        self.pthresh_nth=-1
        #numpy object (persistent registerd) will convert to cupy automatically by to_gpu
        self.aw=np.ones((awLen)  ,dtype=np.float32)
        self.awGrad= np.zeros(self.aw.shape[0],dtype=np.float32) 


        #parameter and grad (Manualy updated)
        self.register_persistent("aw");
        #self.register_persistent("awd"); #too Large and almost no mean
        self.register_persistent("awGrad");
        
        #hyper parameters
        self.register_persistent("pthresh");
        self.register_persistent("pthresh_base");
        self.register_persistent("pthresh_nth");
        self.register_persistent("limit");
        self.register_persistent("eps");
        self.register_persistent("normModel");
        self.register_persistent("L1decay");
        self.register_persistent("decay");
        self.register_persistent("gamma");
        self.register_persistent("updateRate");

            
    def getPFilter(self):
        xp=cupy.get_array_module(self.aw)
        
        if(self.pthresh<=0):
            return xp.ones(self.aw.shape,dtype=xp.float32)
        if(xp!=np):
            aw=cuda.to_cpu(self.aw)
        else:
            aw=self.aw
        sd=np.mean(aw)
        mean=0
        awmin=np.min(aw)
        if(awmin>0.1):
            mean=awmin
            sd-=mean  
        if(self.pthresh_nth>0):
            mean=np.sort(aw)[-self.pthresh_nth]/3

        if(xp==np):
            thresh=xp.random.randn(self.aw.shape[0]).astype(np.float32)
        else:
            thresh=xp.random.randn(self.aw.shape[0],dtype=xp.float32)
        
        thresh*=sd*self.pthresh+mean+sd*self.pthresh_base
        fil=xp.ones(self.aw.shape,dtype=xp.float32)
        fil*=(self.aw>thresh)                                             
        return fil
    
    def __call__(self,h):
        aw=Variable(self.aw)
        if chainer.config.train:
            fil=self.getPFilter()
            self.awd=h*F.broadcast_to(aw*fil,(h.shape))    
        else:            
            self.awd=h*F.broadcast_to(aw,(h.shape))    
        return self.awd
        
    def accGrad(self,predictor,y,gv,skipBackward=False):
        '''
        need forward operation before run this 
        Calling just after update() is recommended        
        '''        
        xp=cupy.get_array_module(self.aw)
        if(not skipBackward):
            predictor.cleargrads()
            y.grad=gv
            y.backward(retain_grad=True)  
        self.awGrad += xp.sum( xp.abs(self.awd.grad.reshape(-1,self.aw.shape[0]))**self.normModel,axis=0)          
            
    def update(self,nextW=None):
        xp=cupy.get_array_module(self.aw)
        if(xp.isnan(self.aw).any()):
            self.aw.fill(1)
        if(not nextW is None):
            orgAw=xp.copy(self.aw)
        
        if(0<self.decay):self.aw*=(1-self.decay) #L2 regulator
        if(0<self.L1decay): 
            self.aw-=self.L1decay #* xp.max(self.aw) #L1 regulator
            self.aw=xp.maximum(self.eps,self.aw)
        if(self.normModel!=1):
            self.awGrad**=1.0/self.normModel
        self.awGrad/=(xp.mean(self.awGrad)+self.eps)
        self.awGrad=xp.maximum(self.eps,self.awGrad)
        self.awGrad/=xp.mean(self.awGrad)
        if(1!=self.updateRate):
            self.awGrad**=self.updateRate 
        self.aw*=self.awGrad


        self.aw**=self.gamma
        
        self.aw/=(xp.mean(self.aw)+self.eps)
        self.aw=xp.maximum(self.eps,self.aw) #to avoid zero   
        self.aw=xp.minimum(self.limit,self.aw) #to avoid zero   

        if(not nextW is None):
            nextW*=orgAw/(self.aw+self.eps)
            #print("rateShape",orgAw.shape,"w shpe",nextW.shape)


        #self.aw+=xp.random.rand(*self.aw.shape)*0.1
        self.awGrad.fill(0)
    def cutOut(self,thresh):
        xp=cupy.get_array_module(self.aw)
        if(xp!=np):self.aw=cuda.to_cpu(self.aw)
        self.aw[self.aw<thresh]*=0
        if(xp!=np):self.aw=cuda.to_gpu(self.aw)
  

def targetToGv(t,nofTar,xp=cupy):
    if(not isinstance(t,np.ndarray)):t=cuda.to_cpu(t)
    b=np.full((len(t),nofTar),-1/(len(t)-1),dtype=np.float32)
    b[ range(len(t)),t]=1
    if(np!=xp):return cuda.to_gpu(b)
    return b

def zeroOneGv(batchsize,xp=cupy):
    gv=xp.ones((batchsize,2),dtype=xp.float32)
    gv[:,0]*=-1
    return gv
        
import matplotlib.pyplot as plt
class Aw_hook:
    '''
    Helper class for Train extension
    '''
    def __init__(self,trainToProp,awViewHook=None,withGraph=0,startIte=0,awIteSpan=5,endIte=1E+100):
        self.__name__="aw_hook"
        self.doAw=0
        self.withGraph=withGraph
        self.awViewHook=awViewHook
        self.trainToProp=trainToProp
        self.startIte=startIte
        self.awIteSpan=awIteSpan
        self.accStart=awIteSpan-1
        self.endIte=endIte
        self.skipBackward=False #if True : Don't use gv. Use gradient of normal Learning.
        self.cutOutEnable=False
        
    def _graph(self,aw):
        #===========debug view
        plt.figure(aw.name)
        plt.clf()
        fil=aw.getPFilter()
        awf=aw.aw*fil  
        awfc=cuda.to_cpu(awf)
        awc=cuda.to_cpu(aw.aw)
        plt.plot(awfc,color="red")
        plt.plot(awc,color="blue")
        #plt.xlim([0,100])
        plt.pause(0.000001)                
        
    def __call__(self,trainer):            
        prop=self.trainToProp(trainer)
        aw=prop.aw
        iteration=trainer.updater.iteration
        #print("Iteration",iteration)
        if(self.cutOutEnable and self.endIte==iteration):
            aw.cutOut(aw.eps*2)
        if(self.endIte<=iteration):
            return
        if(self.startIte <= iteration):#startAccGrad
            self.doAw=self.startIte+self.awIteSpan 
        #accumurate grad every iteration               
        if(self.doAw>0 and  ((iteration%self.awIteSpan) >= self.accStart) ): #accGrad must be start before updating
            aw.accGrad(prop.model,prop.y,prop.gv,skipBackward=self.skipBackward)
        #Update aw each awIteSpan times iteration.
        if((iteration+1)%self.awIteSpan==0 and 1<self.doAw<=iteration): #update
            aw.update(prop.nextW if hasattr(prop,"nextW")  else None )
            if(self.withGraph and (iteration//self.awIteSpan)%self.withGraph==0):
                self._graph(aw)
            if(not self.awViewHook is None):self.awViewHook(aw)
            
            