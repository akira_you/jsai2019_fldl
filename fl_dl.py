#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  

Created on Tue Nov 20 22:03:16 2018

@author: La-noda
"""

import numpy as np
import chainer
import chainer.functions as F
import chainer.links as L
from chainer import cuda, optimizers,Chain,training
import matplotlib.pyplot as plt
import bi
import filtermix

import util
devNo=1
n_med1st=8 #20
import sys
if(not sys.argv[1] is None):n_med1st= int(sys.argv[1])
if(not sys.argv[2] is None):devNo= int(sys.argv[2])

if(0<=devNo):
    cuda.get_device(devNo).use()

nofType=2

d=np.load("data2/haipla.npz")
inputs=d['inputs'].astype(np.float32)#(-1,1,1024,1)
inputs=inputs.reshape(-1,1024)
tars_txt=d['tars_txt']
tars=util.mtar(tars_txt,nofType=nofType)
#del d
fil=np.load("getSpec/filter.npy").astype(np.float32)
vidx=np.nonzero(np.sum(fil,axis=0)>1e-6)[0]
fil=fil[:,vidx]
inputs=inputs[:,vidx]
if(np.min(inputs)<1e-6):
    raise "value range error"
fil=np.log(fil)
inputs=np.log(inputs)



resultDir="FLDL"+str(n_med1st) #+"_"+str(devNo)
n_med=n_med1st
n_medlayer=2
useAw=False
lassoRate1st=1e-5*0
lassoRateFil=1e-4
lassoRate=1e-6
decayRate=0
awLimit=1e+10
batchMode=False

n_epoch = 7000
awStartIteration=128*20
awCount=30 #very important factor trade off of speed and search space 
batchsize=1000
pthresh=1#3


class MyChain(Chain): 
    def __init__(self,fil):
        self.n_input=fil.shape[1]
        self.n_fil=fil.shape[0]

        self.n_med=n_med
        self.n_medlayer=n_medlayer
        super().__init__(
            fil = filtermix.FilterMix(fil,n_med1st),
            fc1 = L.Linear(n_med1st,n_med1st),
            fc2 = chainer.ChainList(),
            fc3 = L.Linear(n_med,nofType),
        )
        for i in range(self.n_medlayer):
            self.fc2.add_link(L.Linear(None,self.n_med))
        
    @chainer.static_graph
    def __call__(self, h):
        h=self.fil(h)
        h=self.fc1(h)
        h=F.elu(h)
    
        h=F.dropout(h)
        for f in self.fc2:
            h=f(h)
            h=F.elu(h)
        h=self.fc3(h)
        h=F.elu(h)
        return h

import chainer
from chainer.training import make_extension
@make_extension((5, 'epoch'))
def reportFC1(trainer):
    fc1=trainer.updater._optimizers['main'].target.predictor.fil
    plt.figure("fc11")
    plt.clf()
    #plt.xlim(0,200)
    #plt.plot(cuda.to_cpu(cupy.linalg.norm(fc1.W.data,axis=0)).transpose())
    plt.plot(cuda.to_cpu(fc1.W.data).transpose())
    plt.pause(0.1)





if __name__=='__main__':
    Tidx=set(range(0,inputs.shape[0],10))
    Lidx=set(range(inputs.shape[0])).difference(Tidx)
    Tidx=list(Tidx)
    Lidx=list(Lidx)
    
    Linputs=inputs[Lidx,:]
    Tinputs=inputs[Tidx,:]
    Ltars=tars[Lidx]
    Ttars=tars[Tidx]
    trainData=chainer.datasets.TupleDataset(Linputs,Ltars)
    testData =chainer.datasets.TupleDataset(Tinputs,Ttars)
    chain=MyChain(fil)
    model=L.Classifier(chain)
    if(0<=devNo):
        model.to_gpu()
    optimizer = optimizers.Adam(alpha=1E-3)
    optimizer.use_cleargrads()
    optimizer.setup(model)
  
    if(lassoRate1st>0):    
        model.predictor.fc1.W.update_rule.add_hook(chainer.optimizer.Lasso(lassoRate1st))
    if(lassoRateFil>0):
        model.predictor.fil.W.update_rule.add_hook(chainer.optimizer.Lasso(lassoRateFil))

    if(decayRate>0):
        optimizer.add_hook(chainer.optimizer.WeightDecay(decayRate))
    if(lassoRate>0):
        optimizer.add_hook(chainer.optimizer.Lasso(lassoRate))
    #train_iter = chainer.iterators.SerialIterator(trainData, batchsize) 
    train_iter = bi.BlanceIterator(trainData, Ltars  ,batchsize)
    test_iter = chainer.iterators.SerialIterator(testData, batchsize,repeat=False) 
    updater = training.StandardUpdater(train_iter,optimizer,device=devNo)
    trainer = training.Trainer(updater,stop_trigger= (n_epoch, 'epoch'),out=resultDir)
 
    import chainer.training.extensions as extensions
    trainer.extend(extensions.LogReport())
    trainer.extend(extensions.Evaluator(test_iter, model, device=devNo))
    trainer.extend(extensions.PrintReport(['epoch', 'main/accuracy', 'validation/main/accuracy' ,'main/loss' ,'validation/main/loss' ,'elapsed_time']))
    trainer.extend(extensions.snapshot(),trigger=(100, 'epoch'))
    if(  not batchMode ):trainer.extend(reportFC1)
    
    #To load some network do like here.
    #chainer.serializers.load_npz("BATCH/Aw_T_Decay_0_Lasso_1e-5_Lasso1_1e-5_P_3_awLimit_1e+10_nLayer_6_nMed_100_nMed1_1000/snapshot_iter_338",trainer)
    
    if(False): 
        model.to_cpu()
        y=model.predictor(Tinputs)
        import chainer.computational_graph as c
        g = c.build_computational_graph(y)
        with open('networ.dot', 'w') as o:
            o.write(g.dump())
    
    
    trainer.run()
    from pathlib import Path
    Path(trainer.out+'/END').touch()
    
    fc1=trainer.updater._optimizers['main'].target.predictor.fc1
    plt.figure("fc11")
    plt.clf()
    plt.plot(cuda.to_cpu(fc1.W.data).transpose())
    plt.savefig(trainer.out+"/fc1.png");
    
