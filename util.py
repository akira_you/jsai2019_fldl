# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


Created on Wed Jun 22 19:38:07 2016

@author: a-noda
"""

import numpy as np
def mtar(tars_txt,nofType=2):
    types={'OTHER':0,'PP':1,'PS':2,'ABS':3,'PUR':4,'CELLULOSE':5,'PE':6,'PC':7,'PA':8,'PVC':9,'EPDM':10,'ABS+PMMA':11}
    types={'OTHER':0,'PP':1,'PS':2,'ABS':3} 
    #types={'OTHER':0,'ABS':1,'PS':2,'PP':3} 
    
    mTar=np.zeros(tars_txt.shape[0],dtype=np.int32)
    for i,x in enumerate(tars_txt):
        if x in types and types[x] < nofType:
          mTar[i]=types[x]
        else:
          mTar[i]=0
    return mTar
class Picker:
    def __init__(self,tars_txt,nofType):
        self.nofType=nofType
        self.tars=[]
        for i in range(nofType):self.tars.append([])
        types={'OTHER':0,'PP':1,'PS':2,'ABS':3} 
        for i,x in enumerate(tars_txt):
            if x in types and types[x] < self.nofType:
              self.tars[types[x]].append(i)
            else:
              self.tars[0].append(i)
        
    def pick(self,num,learn=1):
        return np.array([np.random.choice(self.tars[i][learn::2],num) for i in range(self.nofType) ]).reshape(-1)
    def pickAll(self,learn=1):
        ret=[]
        for i in range(self.nofType):
            ret.extend(self.tars[i][learn::2])
        return np.array(ret)
        

    
