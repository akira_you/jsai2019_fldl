#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  

Created on Fri Nov 30 17:25:01 2018

@author: La-noda
"""
dir="FLDL7"
dir="AWDL"
import numpy as np
import math
import json
import os
import matplotlib.pyplot as plt



with open(dir+"/log") as w:
    log=json.load(w)

for i in range(-1,-20,-1):
    lastsnap=dir+"/snapshot_iter_"+str(log[i]["iteration"])
    if(os.path.exists(lastsnap)):break

res=np.load(lastsnap)




fil=np.load("getSpec/filter.npy").astype(np.float32)
cm=np.linspace(0,7893.48,1024)
vidx=np.nonzero(np.sum(fil,axis=0)>1e-6)[0]
cm=cm[vidx]



plt.close("all")

figsize=(7,5)
mode="fil"
W=res.get("updater/model:main/predictor/fil/W")
if(not W is None):
    fil=res.get("updater/model:main/predictor/fil/fil")
    W[W<0]=0 #simulate relu
    s=W.dot(fil)
    N=W.shape[0]
    ############# not only for 9!!!!!!!
    plt.figure("filter",figsize=figsize)
    for i in range(N):
        plt.tick_params(labelbottom="off")
        plt.xlim((cm[-1],cm[0]))
        if(i-1==N//2):plt.ylabel("Absorbance ")
      
        plt.subplot(N,1,i+1)
        plt.plot(cm,s[i,:]*-1/math.log(10),c="black")
        plt.xlim((cm[-1],cm[0]))
        
    plt.tick_params(labelbottom="on")
    plt.xlabel("Wavenumber [cm-1]")

    plt.savefig("synth_specs.png")



if(W is None):
    mode="aw"
    W=res.get("updater/model:main/predictor/aw/aw")
    if(not W is None):
        plt.figure("aw",figsize=figsize)
        plt.xlim((cm[-1],cm[0]))
        plt.plot(cm,W,c="black")
        plt.xlabel("Wavenumber [cm-1]")
        plt.ylabel("AW weight")
        plt.savefig("aw.png")

    
if(W is None):
    mode="dl"
    W=res.get("updater/model:main/predictor/fc1/W")
    plt.xlim((cm[-1],cm[0]))
 
    plt.figure("dl-fc1w",figsize=figsize)
    plt.plot(cm,np.linalg.norm(W,axis=0),c="black")
    plt.show()
    plt.savefig("dl.png")
    

