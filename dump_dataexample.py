#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  

Created on Fri Feb  1 08:42:17 2019

@author: La-noda
"""
import numpy as np
import math
import json
import os
import matplotlib.pyplot as plt


fil=np.load("getSpec/filter.npy").astype(np.float32)
cm=np.linspace(0,7893.48,1024)
vidx=np.nonzero(np.sum(fil,axis=0)>1e-6)[0]
cm=cm[vidx]


plt.figure("filter",figsize=(7,5))
plt.title("Filter spectrums")
plt.clf()
plt.xlim((cm[-1],cm[0]))
plt.tick_params(labelbottom="off")        
for i in range(fil.shape[0]):
    plt.tick_params(labelleft="off")
    plt.tick_params(labelbottom="on")
    plt.plot(cm,fil[i,vidx]*-1+i,c="black",linewidth=1)
    plt.xlim((cm[-1],cm[0]))
    plt.ylabel("Absorbance (stacked)")
    plt.xlabel("Wavenumber [cm-1]")
plt.savefig("filter.png")


# plot samples examaple
import util
nofType=2
d=np.load("data2/haipla.npz")
inputs=d['inputs'][:,0,vidx,0]
tars_txt=d['tars_txt']
tars=util.mtar(tars_txt,nofType=nofType)

inputsPP=inputs[np.nonzero(tars==1)[0],:]
inputsOTHER=inputs[np.nonzero(tars==0)[0],:]

inputsPP=inputsPP[::inputsPP.shape[0]//15,:]
inputsOTHER=inputsOTHER[::inputsOTHER.shape[0]//15,:]

plt.figure("inputs",figsize=(10,6))
plt.subplot(2,1,1)
plt.title("PP")
for i in range(inputsPP.shape[0]):
    plt.xlim((cm[-1],cm[0]))
    plt.tick_params(labelbottom="off")        
    plt.plot(cm,np.log(inputsPP[i,:])*-1/math.log(10),c="black",linewidth=1)
plt.subplot(2,1,2)
plt.title("OTHER(ABS,PS,PE,etc)")
for i in range(inputsOTHER.shape[0]):
    plt.xlim((cm[-1],cm[0]))
    plt.plot(cm,np.log(inputsOTHER[i,:])*-1/math.log(10),c="black",linewidth=1)
    plt.ylabel("Refrected light intensity")
    plt.xlabel("Wavenumber [cm-1]")
plt.savefig("inputs.png")
