#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  

Created on Tue Nov 20 22:03:16 2018

@author: La-noda
"""

import numpy as np
import chainer
import chainer.functions as F
import chainer.links as L
from chainer import cuda, optimizers,Chain,training
from awLayer import Aw,Aw_hook,zeroOneGv,targetToGv
import matplotlib.pyplot as plt
import bi
devNo=1
nofType=2
cuda.get_device(devNo).use()

resultDir="DL"
n_epoch = 7000
n_med1st=100 #20
n_med=10
n_medlayer=2
useAw=True
if(useAw):resultDir="AWDL"
if(useAw):n_epoch=1000

lassoRate1st=1e-5
lassoRate=1e-6
decayRate=0
awLimit=1e+10
batchMode=False

awStartIteration=128*20
awCount=30 #very important factor trade off of speed and search space 
batchsize=800
pthresh=1#3


class MyChain(Chain): 
    def __init__(self,n,useAw):
        self.n_input=n
        self.n_med=n_med
        self.n_medlayer=n_medlayer
        self.useAw=useAw
        super().__init__(
            fc1 = L.Linear(self.n_input,n_med1st),
            fc2 = chainer.ChainList(),
            fc3 = L.Linear(n_med,nofType),
        )
        for i in range(self.n_medlayer):
            self.fc2.add_link(L.Linear(None,self.n_med))
        if(self.useAw):
            self.add_link("aw",Aw(self.n_input))
            self.aw.updateRate=0.1#05#0.01 #very important factor  tradeoff  of speed and search space
            self.aw.L1decay=lassoRate1st
            self.aw.awStartIteration=awStartIteration
            self.aw.pthresh=pthresh
            self.aw.pthresh_base=-0.5
            self.aw.skipBackward=False
            self.aw.limit=awLimit
      
    def __call__(self, h):
        if(self.useAw):h=self.aw(h)
        h=self.fc1(h)
        h=F.elu(h)
    
        h=F.dropout(h)
        for f in self.fc2:
            h=f(h)
            h=F.elu(h)
        h=self.fc3(h)
        h=F.elu(h)
        return h


import cupy
from chainer.training import make_extension
@make_extension((1, 'epoch'))
def reportFC1(trainer):
    fc1=trainer.updater._optimizers['main'].target.predictor.fc1
    plt.figure("fc11")
    plt.clf()
    #plt.xlim(0,200)
    plt.plot(cuda.to_cpu(cupy.linalg.norm(fc1.W.data,axis=0)).transpose())
    plt.pause(0.1)



import util

if __name__=='__main__':


    d=np.load("data2/haipla.npz")
    inputs=d['inputs'].astype(np.float32)#(-1,1,1024,1)
    inputs=inputs.reshape(-1,1024)
    tars_txt=d['tars_txt']
    tars=util.mtar(tars_txt,nofType=nofType)
    #del d
    fil=np.load("getSpec/filter.npy").astype(np.float32)
    vidx=np.nonzero(np.sum(fil,axis=0)>1e-6)[0]
    fil=fil[:,vidx]
    inputs=(inputs[:,vidx]).reshape(inputs.shape[0],-1)
    if(np.min(inputs)<1e-6):
        raise "value range error"

    Tidx=set(range(0,inputs.shape[0],10))
    Lidx=set(range(inputs.shape[0])).difference(Tidx)
    Tidx=list(Tidx)
    Lidx=list(Lidx)
    
    Linputs=inputs[Lidx,:]
    Tinputs=inputs[Tidx,:]
    Ltars=tars[Lidx]
    Ttars=tars[Tidx]
    trainData=chainer.datasets.TupleDataset(Linputs,Ltars)
    testData =chainer.datasets.TupleDataset(Tinputs,Ttars)
    del d
    

    chain=MyChain(Linputs.shape[1],useAw)
    model=L.Classifier(chain)
    model.to_gpu()
    optimizer = optimizers.Adam()
    optimizer.use_cleargrads()
    optimizer.setup(model)
    #model.predictor.fc1.W.update_rule.add_hook(chainer.optimizer.GradientNoise(1.0))
    if(not useAw):
        model.predictor.fc1.W.update_rule.add_hook(chainer.optimizer.Lasso(lassoRate1st))

    optimizer.add_hook(chainer.optimizer.WeightDecay(decayRate))
    optimizer.add_hook(chainer.optimizer.Lasso(lassoRate))
    train_iter = chainer.iterators.SerialIterator(trainData, batchsize) 
    train_iter = bi.BlanceIterator(trainData, Ltars  ,batchsize)
    acc_iter=chainer.iterators.SerialIterator(Linputs,batchsize)
    test_iter = chainer.iterators.SerialIterator(testData, batchsize,repeat=False) 
    updater = training.StandardUpdater(train_iter,optimizer,device=devNo)
    trainer = training.Trainer(updater,stop_trigger= (n_epoch, 'epoch'),out=resultDir)
 

    if(useAw):
        #In case of Binary discrimination gv can be constant value
        gvZeroOne=zeroOneGv(batchsize)
        class TrainToProp:
            def __init__(self,trainer):
                #fc1=trainer.updater._optimizers['main'].target.predictor.fc1.W
                #fc1.data+100e-4*cupy.random.randn(fc1.data.shape[0],fc1.data.shape[1],dtype=fc1.data.dtype)

                self.aw =    trainer.updater._optimizers['main'].target.predictor.aw 
                self.model = trainer.updater._optimizers['main'].target.predictor
                self.y =     trainer.updater._optimizers['main'].target.y
                #if you want to make gv from data(i.e. 3or more class )
                #use targetToGv(trainer.updater._optimizers['main'].target.loss.creator.inputs[1].data, nofdata)
                self.nextW=None
                #self.nextW= trainer.updater._optimizers['main'].target.predictor.fc1.W
                if('main/accuracy' in trainer.observation):
                    newGamma=0.1+trainer.observation['main/accuracy'].data                
                    newGamma**=0.5
                    #newGamma=newGamma+0.1
                    if(newGamma>0.9999):newGamma=0.9999
                    if(newGamma<0.5):newGamma=0.5
                    self.aw.gamma=newGamma *0.3+0.7*self.aw.gamma
                else:
                    self.aw.gamma=0.5
            
                if(nofType==2):
                    self.gv=  gvZeroOne
                else:   
                    self.gv=targetToGv(trainer.updater._optimizers['main'].target.loss.creator.inputs[1].data,nofType)
        aw_hook=Aw_hook(TrainToProp,startIte=awStartIteration,awIteSpan=awCount,withGraph=0 if batchMode else 2)
        #This extention must run before Evalutor. Becaouse aw_hoook reuse the y(forward result)            
        trainer.extend(make_extension((1, 'iteration'))(aw_hook) ,priority=chainer.training.PRIORITY_WRITER+99)        
    
    import chainer.training.extensions as extensions
    trainer.extend(extensions.LogReport())
    trainer.extend(extensions.Evaluator(test_iter, model, device=devNo))
    trainer.extend(extensions.PrintReport(['epoch', 'main/accuracy', 'validation/main/accuracy' ,'main/loss' ,'validation/main/loss' ,'elapsed_time']))
    trainer.extend(extensions.snapshot(),trigger=(10, 'epoch'))
    if( (not useAw) and (not batchMode) ):trainer.extend(reportFC1)
    
    #To load some network do like here.
    #chainer.serializers.load_npz("BATCH/Aw_T_Decay_0_Lasso_1e-5_Lasso1_1e-5_P_3_awLimit_1e+10_nLayer_6_nMed_100_nMed1_1000/snapshot_iter_338",trainer)
    
    trainer.run()
    from pathlib import Path
    Path(trainer.out+'/END').touch()
    
    fc1=trainer.updater._optimizers['main'].target.predictor.fc1
    plt.figure("fc11")
    plt.clf()
    plt.plot(cuda.to_cpu(fc1.W.data).transpose())
    plt.savefig(trainer.out+"/fc1.png");
    if(useAw):
        aw=trainer.updater._optimizers['main'].target.predictor.aw
        plt.figure("fc11")
        plt.clf()
        plt.plot(cuda.to_cpu(aw.aw).transpose())
        plt.savefig(trainer.out+"/aw.png");
        


